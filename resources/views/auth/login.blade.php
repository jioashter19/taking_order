@extends('layouts.loginlayout')

@section('content')
    <div class="container">
    

    <!-- Outer Row -->
    <div class="row justify-content-center">
    

      <div class="col-xl-10 col-lg-12 col-md-9">
      

        <div class="card o-hidden border-0 shadow-lg my-5">
        
          <div class="card-body p-0">
          
            <!-- Nested Row within Card Body -->
            <div class="row">
            
              <div class="col-lg-6 d-none d-lg-block" id="bg"><br>
                <h5 >
                  <span style="color: #ffcc99; font-weight: bold; margin-left: 30px;
                  ">
                    <i class="fa fa-dice-d6"></i> GO-TRACK
                  </span>
                </h5>
              </div>
              <div class="col-lg-6">
              
                <div class="p-5">
                
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login here, please!</h1>
                    <img src="{{asset('asset/img/icons/acount_login_1172579.png')}}" style="border-radius: 50%; width: 100px; height: 100px;" alt="Avatar">
                  </div>
                  <br>
                  <form method="POST" action="{{ route('login') }}" class="user">
                    @csrf

                    <div class="form-group">
                      <input id="email" type="email" class="form-control form-control-user{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Email Address" required autofocus>

                      @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>

                    <div class="form-group">
                      <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                      @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                      <hr>
                    </div>
                    <button type="submit" class="btn btn-google btn-user btn-sm" style="float: right">
                      <i class="fas fa-unlock-alt"></i> Login</button>
                  </form>

                   <div class="form-group">
                        <!-- Forgot Link -->
                        @if (Route::has('password.request'))
                        <p class="text-xs">
                                    <a href="{{ route('password.request') }}">
                                        Forgot Password?
                                    </a>
                                  </p>
                                @endif

                        <!-- Register Link -->
                        @if (Route::has('register'))
                        <p class="text-xs"> Don't have an account?
                            <a href="{{ route('register') }}"><b>Sign up now!</b></a>
                          </p>
                        @endif
                    </div>
                  <!-- End Register Link -->
          </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection




