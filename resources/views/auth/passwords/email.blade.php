@extends('layouts.resetpasswordlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="padding-top: 60px;">
            <div class="card">
                <div class="card-header" style="background-color: black; color: white;">Reset Passwords</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">
                                E-Mail Address
                            </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-facebook btn-user btn-sm" style="float: right">
                                    <i class="fas fa-sync-alt"></i> Reset Password 
                                </button>
                                <a href="{{url('/')}}" class="btn btn-google btn-user btn-sm" style="float: right">
                                    <i class="fas fa-hand-point-left"></i> Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <!-- Footer -->
                      <footer class="sticky-footer bg-white">
                        <div class="container my-auto">
                          <div class="copyright text-center my-auto">
                            <span>Copyright &copy; BabyDev. 2019 | </span>
                              <a href="#"><i class="fa fa-dribbble"></i></a>
                              <a href="#"><i class="fa fa-twitter"></i></a>
                              <a href="#"><i class="fa fa-linkedin"></i></a>
                              <a href="#"><i class="fa fa-facebook"></i></a>
                          </div>
                        </div>
                      </footer>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
