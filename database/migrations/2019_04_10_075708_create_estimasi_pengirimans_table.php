<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimasiPengirimansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimasi_pengirimans', function (Blueprint $table) {
            $table->bigIncrements('id_estimasi');
            $table->string('kode_agen_kirim');
            $table->string('nm_agen_kirim');
            $table->string('keterangan_estimasi_kirim');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimasi_pengirimans');
    }
}
