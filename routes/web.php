<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/menu1', 'HomeController@menu1')->name('menu1');
Route::post('/home/menu1create', 'HomeController@menu1create')->name('menu1create');
Route::get('/home/{id}/menu1edit', 'HomeController@menu1edit')->name('menu1edit');
Route::post('/home/{id}/menu1update', 'HomeController@menu1update')->name('menu1update');
Route::get('/home/{id}/menu1delete', 'HomeController@menu1delete')->name('menu1delete');

Route::get('/home/kategori', 'HomeController@kategoribrg')->name('kategoribrg');
Route::post('/home/kategoricreate', 'HomeController@kategoricreate')->name('kategoricreate');
Route::get('/home/{id}/kategoriedit', 'HomeController@kategoribrgedit')->name('kategoriedit');
Route::post('/home/{id}/kategoriupdate', 'HomeController@kategoribrgupdate')->name('kategoriupdate');
Route::get('/home/{id}/kategoridelete', 'HomeController@kategoridelete')->name('kategoridelete');

Route::get('/home/menu2', 'HomeController@menuke2')->name('menu2');
Route::post('/home/menu2create', 'HomeController@menu2create')->name('menu2create');
Route::get('/home/{id}/menu2edit', 'HomeController@menu2edit')->name('menu2edit');
Route::post('/home/{id}/menu2update', 'HomeController@menu2update')->name('menu2update');
Route::get('/home/{id}/menu2delete', 'HomeController@menu2delete')->name('menu2delete');

Route::get('/home/menu3', 'HomeController@menuke3')->name('menuke3');
Route::post('/home/menu3create', 'HomeController@menuke3create')->name('menu2create');
Route::get('/home/{id}/menu3edit', 'HomeController@menuke3edit')->name('menu3edit');
Route::post('/home/{id}/menu3update', 'HomeController@menuke3update')->name('menu3update');
Route::get('/home/{id}/menu3delete', 'HomeController@menuke3delete')->name('menu2delete');


Route::get('/home/lokasi', 'HomeController@lokasi')->name('lokasi');
Route::post('/home/lokasicreate', 'HomeController@lokasicreate')->name('lokasicreate');
Route::get('/home/{mlokasi}/lokasiedit', 'HomeController@lokasiedit')->name('lokasiedit');
Route::post('/home/{mlokasi}/lokasiupdate', 'HomeController@lokasiupdate')->name('lokasiupdate');
Route::get('/home/{mlokasi}/lokasidelete', 'HomeController@lokasidelete')->name('lokasidelete');

Route::get('/home/optmenu1', 'HomeController@optmenu1')->name('optmenu1');
Route::get('/home/optmenu2', 'HomeController@optmenu2')->name('optmenu2');
Route::get('/home/{id}/optmenu2delete', 'HomeController@optmenu2delete')->name('optmenu2delete');

Route::get('/home/optmenu3', 'HomeController@optmenu3')->name('optmenu3');
Route::get('/home/{id}/optmenu3delete', 'HomeController@optmenu3delete')->name('optmenu3delete');
Route::get('/home/{id}/detailcheckout', 'HomeController@optmenu3detailcheckout')->name('optmenu3detailcheckout');

Route::get('/home/tracking', 'HomeController@tracking')->name('tracking');
Route::get('/home/{user}/profil', 'HomeController@profil')->name('profil');
Route::get('/home/{user}/editprofil', 'HomeController@editprofil')->name('editprofil');
Route::post('/home/{user}/profilupdate', 'HomeController@profilupdate')->name('profilupdate');

Route::get('/home/grafik', 'HomeController@grafik')->name('grafik');
Route::get('/home/tabel', 'HomeController@tabel')->name('tabel');
Route::get('/home/errorpage404', 'HomeController@errorpage404')->name('errorpage404');

Route::get('/home/track', 'HomeController@track')->name('track');
