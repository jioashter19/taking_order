<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MKategori extends Model
{
    protected $table = 'kategoris';
    protected $fillable=['id', 'keterangan_kategori', 'created_at', 'updated_at'];
}
