<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use App\MLokasi;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('home');
    // }
    public function index()
    {
        $data=DB::select('select count(id) from users');
        // return $data;
        return view('dashboard.home',['data'=>$data]);
    }

    //* Menu1 = Data Barang
    public function menu1()
    {
         $data1=DB::select('select * from kategoris');
        $data = \App\MBarang::all();
        return view('dashboard.datamaster.menu1',['data'=> $data],['data1'=> $data1]);
    }
    public function menu1edit($id)
    {
        $data = \App\MBarang::find($id);
        return view('dashboard.datamaster.menu1edit',['data'=> $data]);
    }
    public function menu1update(Request $request, $id)
    {
        $data = \App\MBarang::find($id);
        $data->update($request->all());
        return redirect('/home/menu1');
    }
    public function menu1create(Request $request)
    {
        // dd($request->all());
        $data = \App\MBarang::create($request->all());
        if($request->hasFile('image')){
            $request->file('image')->move('asset/img/',$request->file('image')->getClientOriginalName());
            $data->image = $request->file('image')->getClientOriginalName();
            $data->save();
        }
        return redirect('/home/menu1');
    }
    public function menu1delete($id)
    {
        $data = \App\MBarang::find($id);
        $data->delete($data);
        // alert()->success('Data telah ditambahkan','BERHASIL')->persistent('Ok');
        return redirect('/home/menu1');
    }
    //* Menu1 = Data Barang

    //* Menu Kategori Barang
    public function kategoribrg()
    {
        $data = \App\MKategori::all();
        return view('dashboard.datamaster.kategori',['data'=> $data]);
    }

    public function kategoricreate(Request $request)
    {
        \App\MKategori::create($request->all());
        return redirect('/home/kategori');
    }

    public function kategoribrgedit($id)
    {
        $data = \App\MKategori::find($id);
        return view('dashboard.datamaster.kategori_edit',['data'=> $data]);
    }

    public function kategoribrgupdate(Request $request, $id)
    {
        $data = \App\MKategori::find($id);
        $data->update($request->all());
        return redirect('/home/kategori');
    }
    
    public function kategoridelete($id)
    {
        $data = \App\MKategori::find($id);
        $data->delete($data);
        return redirect('/home/kategori');
    }
    // Menu Kategori Barang

    //* Menu2 = Outlet
    public function menuke2()
    {
        // $data_brg=DB::select('select * from barangs order by id desc');
        $data_lokasi=DB::select('select * from lokasis order by nm_area asc');
        $data = \App\MOutlet::all();
        return view('dashboard.datamaster.menu2',['data'=> $data],['data_lokasi'=> $data_lokasi]);
    }

    public function menu2edit($id)
    {
        $data_lokasi=DB::select('select * from lokasis order by nm_area asc');
        $data = \App\MOutlet::find($id);
        return view('dashboard.datamaster.menu2edit',['data'=> $data],['data_lokasi'=> $data_lokasi]);
    }

    public function menu2update(Request $request, $id)
    {
        $data = \App\MOutlet::find($id);
        $data->update($request->all());
        return redirect('/home/menu2');
    }

    public function menu2create(Request $request)
    {
        \App\MOutlet::create($request->all());
        return redirect('/home/menu2');
    }

    public function menu2delete($id)
    {
        $data = \App\MOutlet::find($id);
        $data->delete($data);
        return redirect('/home/menu2');
    }
    //* Menu2 = Outlet

    //* Menu3 = Pembayaran
    public function menuke3()
    {
        /* $data1=DB::select('select * from kategoris');*/
        $data = \App\MBayar::all();
        return view('dashboard.datamaster.menu3',['data'=> $data]);
    }
    public function menuke3edit($id)
    {
        $data = \App\MBayar::find($id);
        return view('dashboard.datamaster.menu3edit',['data'=> $data]);
    }
    public function menuke3update(Request $request, $id)
    {
        $data = \App\MBayar::find($id);
        $data->update($request->all());
        return redirect('/home/menu3');
    }
    public function menuke3create(Request $request)
    {
        \App\MBayar::create($request->all());
        return redirect('/home/menu3');
    }
    public function menuke3delete($id)
    {
        $data = \App\MBayar::find($id);
        $data->delete($data);
        return redirect('/home/menu3');
    }
    //* Menu3 = Pembayaran

    //* Menu Lokasi
    public function lokasi()
    {
        $data = \App\MLokasi::all();
        return view('dashboard.datamaster.lokasi',['data' => $data]);
    }

    public function lokasicreate(Request $request)
    {
        \App\MLokasi::create($request->all());
        return redirect('/home/lokasi');
    }

    //Ini Contoh model binding
    public function lokasiedit(MLokasi $mlokasi)
    {
        // $data = \App\MLokasi::find($id);
        return view('dashboard.datamaster.lokasiedit',['data'=> $mlokasi]);
    }

    public function lokasiupdate(Request $request,$id)
    {
        $data = MLokasi::find($id);
        $data->update($request->all());
        return redirect('/home/lokasi');
    }

    // public function lokasidelete($id)
    // {
    //     $data =\App\MLokasi::find($id);
    //     $data->delete($data);
    //     return redirect('/home/lokasi');
    // }
    public function lokasidelete(MLokasi $mlokasi)
    {
        // $data =\App\MLokasi::find($id);
        $mlokasi->delete($mlokasi);
        return redirect('/home/lokasi');
    }
    //End Lokasi

    // optmenu1 = Order Barang
    public function optmenu1()
    {
        $data = \App\MBarang::all();
        return view('dashboard.othersmenu.optionmenu1',['data' => $data]);
    }
    // optmenu1 = Order Barang

    //* optmenu1 = Cart Barang
    public function optmenu2delete($id)
    {   
        $data = \App\MBarang::find($id);
        $data->delete($data);
        // $data = \App\MBarang::all();
        return view('dashboard.othersmenu.optionmenu2',['data' => $data]);
    }

//     public function optmenu2($id)
//     {   
//         $usercart = \App\MTroli::find($id);
//         $usercart=DB::select("select a.id_cart, a.id_outlet, b.nm_outlet, a.id, c.name, a.created_at as pemesanan_dibuat, d.*
// from cartpesans a join outlets b on a.id_outlet=b.id join users c on a.id=c.id join barangs
// WHERE a.id='$id' order by a.created_at desc");
//         // $data = \App\MTroli::all();
//         return view('dashboard.othersmenu.optionmenu2',['data' => $data],['usercart' => $usercart]);
//     }

    public function optmenu2()
    {   
        $data = \App\MTroli::all();
        // $data = \App\MBarang::all();
        return view('dashboard.othersmenu.optionmenu2',['data' => $data]);
    }
    // optmenu1 = Cart Barang

    // Checkout
    public function optmenu3()
    {
        $data = \App\MCheckout::all();
        return view('dashboard.othersmenu.optionmenu3',['data' => $data]);
    }

    public function optmenu3delete($id)
    {   
        $data = \App\MCheckout::find($id);
        $data->delete($data);
        // $data = \App\MBarang::all();
        return view('dashboard.othersmenu.optionmenu3',['data' => $data]);
    }

    public function optmenu3detailcheckout($id)
    {
        // dd($request->all($id));
        // $data1 = \App\MCheckout::find($id);
        // $data=DB::select('select a.id,a.id_cart, a.id_pembayaran, b.keterangan_pembayaran, a.id_estimasi, c.nm_agen_kirim, c.keterangan_estimasi_kirim, a.created_at FROM checkouts a, metode_pembayarans b, estimasi_pengirimans c WHERE a.id_pembayaran=".$id." GROUP by a.id_cart');
        // return $data;
        // return view('dashboard.othersmenu.optmn3detailcheckout',['data' => $data],['data' => $data1]);
    }
    // End Checkout

    public function grafik()
    {
        return view('dashboard.grafik.datagrafik');
    }

    public function tabel()
    {
        return view('dashboard.tabledata.tabelindex');
    }

    public function errorpage404()
    {
        return view('dashboard.page404.404page');
    }

    public function tracking()
    {
        return view('dashboard.othersmenu.tracking');
    }

    //Coba
    public function track()
    {
        return view('dashboard.othersmenu.track');
    }

    public function profil(User $user)
    {
        return view('dashboard.profil.profil',['data'=>$user]);
    }

    public function editprofil(User $user)
    {
        // $data = \App\user::Find();
        return view('dashboard.profil.editprofil',['data'=>$user]);
    }

    public function profilupdate(Request $request, User $user)
    {
        // $data = User::find($id);
        $user->update($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('asset/img/userimage/',$request->file('foto')->getClientOriginalName());
            $user->foto = $request->file('foto')->getClientOriginalName();
            $user->save();
        }
        // return redirect('/home/profil',['data'=>$user]);
        return view('dashboard.profil.profil',['data'=>$user]);
    }

    

}
