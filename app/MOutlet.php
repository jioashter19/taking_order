<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MOutlet extends Model
{
    protected $table = 'outlets';
    protected $fillable=['id','nm_outlet','id_barang','id_lokasi','created_at'];
}
