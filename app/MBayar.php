<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MBayar extends Model
{
    protected $table = 'metode_pembayarans';
    protected $fillable=['id', 'keterangan_pembayaran','created_at','updated_at'];
}
