<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MTroli extends Model
{
    protected $table = 'cartpesans';
    protected $fillable=['id_cart','id_outlet','id', 'created_at','updated_at'];
}
