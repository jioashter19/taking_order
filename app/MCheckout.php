<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MCheckout extends Model
{
    protected $table = 'checkouts';
    protected $fillable=['id', 'id_cart', 'id_pembayaran', 'id_estimasi', 'created_at', 'updated_at'];
}
