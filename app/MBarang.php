<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MBarang extends Model
{
    protected $table = 'barangs';
    protected $fillable=['id_barang','nm_barang','keterangan_barang','id_kategori','harga','image','created_at','updated_at'];
}
