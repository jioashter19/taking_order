<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MLokasi extends Model
{
    protected $table = 'lokasis';
    protected $fillable=['id', 'nm_area', 'created_at', 'updated_at'];
}
